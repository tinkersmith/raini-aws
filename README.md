# Raini AWS Extension



## Install

This extension requires the `raini/core` Composer package, which
provides a setup for.

## Configurations

Available configurations for the Raini AWS extension:

 * **accessFile:** - File containing the s3 access keys and private information.
   This is used to keep the access information outside of the settings file. If
   access is provided through EC2 instance IAM grants the access keys aren't
   required as they are provided though the AWS environment.
 * **s3Bucket:** - The default S3 bucket to use for S3 related commands.
 * **awsRegion:** - The AWS region to use for SecretsManager and S3 resources,
   this value is not needed on AWS / EC2 instances since they can detect the
   region automatically.

Example configurations (raini.project.yml):

```
extensions:
    raini-aws:
        accessFile: .keys/awsKeys.env
        includeSettings: false
```
