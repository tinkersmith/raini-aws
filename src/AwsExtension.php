<?php

/*
 * This file is part of the Raini AWS package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\AWS;

use Raini\Core\Configurator\ExtensionSchemaConfigurator;
use Raini\Core\Extension\AbstractExtension;
use Raini\Core\Extension\ComposerExtensionInterface;
use Tinkersmith\Configurator\Attribute\Configurator;

/**
 * The extension handler class for the AWS extension.
 */
#[Configurator(ExtensionSchemaConfigurator::class, 'raini-aws', 'config/aws.schema.yml')]
class AwsExtension extends AbstractExtension implements ComposerExtensionInterface
{

    /**
     * {@inheritdoc}
     */
    public function getRepositories(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getPackages(): array
    {
        // Ensure that the AWS package is include in all the tenants so they
        // can utilize the value resolver expression, without needing the full
        // raini packages.
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getDevPackages(): array
    {
        return [];
    }
}
