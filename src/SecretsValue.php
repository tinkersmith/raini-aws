<?php

/*
 * This file is part of the Raini AWS package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\AWS;

use AsyncAws\SecretsManager\SecretsManagerClient;

/**
 * Fetch AWS secrets from AWS secrets manager services.
 */
class SecretsValue extends AwsValueProvider
{
    const AWS_SERVICE='SECRET_MANGER';
    const CLIENT_CLASS=SecretsManagerClient::class;

    /**
     * Static cache of currently loaded secrets.
     *
     * Keeps the currently loaded values for this Raini request. These values
     * only last for the current duration of the Raini instance and do not
     * persist locally.
     *
     * @var mixed[]
     */
    protected static array $secrets = [];

    /**
     * The AWS service client to make API calls through.s
     *
     * @var SecretsManagerClient[]
     */
    protected static array $client = [];

    /**
     * Get the secret with the matching keyname on this system.
     *
     * @param string  $keyname  The SecretID or secret identifier to fetch.
     * @param mixed[] $settings File path to the AWS credentials and secrets settings.
     * @param bool    $reload   Should the secret be reloaded regardless of whether or not it was already loaded.
     *
     * @return mixed[] An array carrying the AWS secret information with the "values" being the decoded JSON values an
     *                 the information like ARN, name, created date and version as values of the array.
     *
     * @throws ResourceNotFoundException
     * @throws DecryptionFailureException
     * @throws InternalServiceErrorException
     */
    public static function getSecret(string $keyname, array $settings, bool $reload = false): array
    {
        if (!isset(static::$secrets[$keyname]) || $reload) {
            /** @var SecretsManagerClient $manager */
            $manager = static::getClient($settings);
            $secret = $manager->getSecretValue([
                'SecretId' => $keyname,
            ]);

            static::$secrets[$keyname] = [
                'arn' => $secret->getArn(),
                'name' => $secret->getName(),
                'created' => $secret->getCreatedDate(),
                'version' => $secret->getVersionId(),
                'binary' => $secret->getSecretBinary(),
                'values' => json_decode($secret->getSecretString(), true),
            ];
        }

        return static::$secrets[$keyname];
    }
}
