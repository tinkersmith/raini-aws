<?php

/*
 * This file is part of the Raini AWS package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\AWS;

use AsyncAws\Ssm\SsmClient;

/**
 * Fetch parameters from AWS systems manager services.
 */
class SSMParameter extends AwsValueProvider
{
    const AWS_SERVICE='SSM';
    const CLIENT_CLASS=SsmClient::class;

    /**
     * List of fetched secrets keyed by their SecretID.
     *
     * Fetching and resolving secrets are potentially expensive, so cache the
     * values that have been fetch so far to fast retrieval.
     *
     * @var mixed[]
     */
    protected static array $params = [];

    /**
     * Get the parameters by path from the AWS Systems Manager services.
     *
     * @param string  $path      The parameter path to fetch.
     * @param array   $settings  The AWS extension settings to use to connect with the SSM service.
     * @param boolean $recursive Recursively load parameter values?
     * @param boolean $reload    Should previously fetched values be forced to reload.
     *
     * @return mixed[] The parameter values matching the provided path.
     *
     * @throws ResourceNotFoundException
     * @throws DecryptionFailureException
     * @throws InternalServiceErrorException
     */
    public static function getParameters(string $path, array $settings, bool $recursive = false, bool $reload = false): array
    {
        if (!isset(static::$params[$path]) || $reload) {
            /** @var SsmClient $client */
            $client = static::getClient($settings);
            $params = $client->getParametersByPath([
                'Path' => $path,
                'Recursive' => $recursive,
                'WithDecryption' => true,
            ]);

            static::$params[$path] = [];
            foreach ($params as $param) {
                static::$params[$path][$param->getName()] = $param;
            }
        }

        return static::$params[$path];
    }
}
