<?php

/*
 * This file is part of the Raini AWS package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\AWS;

use AsyncAws\Core\AbstractApi;
use Raini\Core\Utility\EnvValues;

/**
 * Base class for fetching values from an AWS service.
 */
abstract class AwsValueProvider
{
    const AWS_SERVICE='ACCESS';
    const CLIENT_CLASS='';

    protected static array $client;

    /**
     * Gets the service client for this ValueResolver.
     *
     * The client fetch is specific for each value resolver and is dependant
     * on the service where the resolver fetches AWS data from.
     *
     * @param mixed[] $settings The AWS extension settings, and connection settings.
     *
     * @return AbstractApi An Async AWS service client to make the AWS API calls through.
     */
    #[\ReturnTypeWillChange]
    protected static function getClient(array $settings): AbstractApi
    {
        @$clientKey = $settings['accessFile'] ?: "{$settings['awsRegion']}:{$settings['accessKeyId']}";

        if (!isset(static::$client[$clientKey])) {
            $conn = [];
            $conn['region'] = $settings['awsRegion'] ?? null;

            if (!empty($settings['accessFile']) && file_exists($settings['accessFile'])) {
                if ($vars = EnvValues::createFromFile($settings['accessFile'])) {
                    // AWS IAM general service access key and secret from file.
                    $conn['accessKeyId'] = $vars['AWS_'.static::AWS_SERVICE.'_KEY'] ?? $vars['AWS_ACCESS_KEY'];
                    $conn['accessKeySecret'] = $vars['AWS_'.static::AWS_SERVICE.'_SECRET'] ?? $vars['AWS_ACCESS_SECRET'];

                    if (empty($conn['region']) && isset($vars['AWS_REGION'])) {
                        $conn['region'] = $vars['AWS_REGION'];
                    }
                }
            } else {
                $conn['accessKeyId'] = $settings['accessKeyId'] ?? null;
                $conn['accessKeySecret'] = $settings['accessKeySecret'] ?? null;
            }

            static::$client[$clientKey] = new (static::CLIENT_CLASS)(array_filter($conn));
        }

        return static::$client[$clientKey];
    }
}
