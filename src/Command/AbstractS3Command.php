<?php

/*
 * This file is part of the Raini AWS package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\AWS\Command;

use AsyncAws\S3\S3Client;
use Raini\Core\Extension\ExtensionInterface;
use Raini\Core\Extension\ExtensionManager;
use Raini\Core\Utility\EnvValues;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * The base class implementation for S3 based commands.
 */
abstract class AbstractS3Command extends Command
{

    /**
     * The Raini AWS extension.
     *
     * @var ExtensionInterface
     */
    protected ExtensionInterface $extension;

    /**
     * @param ExtensionManager $extensionManager
     */
    public function __construct(ExtensionManager $extensionManager)
    {
        $this->extension = $extensionManager->getExtension('raini-aws');

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $bucket = $this->extension->getSettings()['s3Bucket'] ?? null;
        if ($bucket) {
            $mode = InputOption::VALUE_OPTIONAL;
            $bucketDesc = sprintf('The S3 bucket to upload the file to. If excluded the default S3 bucket "%s" is used.', $bucket);
        } else {
            $mode = InputOption::VALUE_REQUIRED;
            $bucketDesc = 'The S3 bucket to upload the file to. Set the default bucket by setting "s3Bucket" value in the raini.project.yml file.';
        }

        $this->addOption('bucket', 'b', $mode, $bucketDesc);
    }

    /**
     * @param InputInterface $input The command inputs.
     *
     * @return string The S3 bucket to use.
     *
     * @throws \InvalidArgumentException If an S3 bucket isn't available.
     */
    protected function getBucket(InputInterface $input): string
    {
        $settings = $this->extension->getSettings();
        $bucket = $input->getOption('bucket') ?? $settings['s3Bucket'] ?? null;

        // Without a bucket specified we can't perform any S3 operations.
        if (empty($bucket)) {
            throw new \InvalidArgumentException('Missing S3 bucket. Use the "--bucket" option or provide one in the raini.project.yml settings file.');
        }

        return $bucket;
    }

    /**
     * Get the S3 client to make S3 request and actions with.
     *
     * The settings come from the Raini AWS settings and the credentials file
     * (specified by the "accessFile" setting). It is recommend to place the
     * access file in a secure place and to use the Linux environment variables
     * value format.
     *
     * @return S3Client The S3Client to run the S3 based command with.
     */
    protected function getS3Client(): S3Client
    {
        $settings = $this->extension->getSettings();
        $conn = ['region' => $settings['awsRegion'] ?? null];

        if (!empty($settings['accessFile']) && file_exists($settings['accessFile'])) {
            $vars = EnvValues::createFromFile($settings['accessFile']);

            // Prefer and S3 prefixed variable, but fallback to the general
            // AWS IAM general service access key and secret.
            $conn['accessKeyId'] = $vars['S3_ACCESS_KEY'] ?: $vars['AWS_ACCESS_KEY'];
            $conn['accessKeySecret'] = $vars['S3_ACCESS_SECRET'] ?: $vars['AWS_ACCESS_SECRET'];

            if (empty($conn['region']) && isset($vars['AWS_REGION'])) {
                $conn['region'] = $vars['AWS_REGION'];
            }
        }

        return new S3Client(array_filter($conn));
    }
}
