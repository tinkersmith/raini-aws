<?php

/*
 * This file is part of the Raini AWS package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\AWS\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command for uploading a file to an S3 bucket.
 */
#[AsCommand(
    name: 'aws:s3upload',
    description: 'Uploads a file or resource to S3 storage bucket.',
    aliases: ['s3up'],
)]
class S3UploadCommand extends AbstractS3Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->addOption('s3name', null, InputOption::VALUE_OPTIONAL, 'Name to use for the file on the S3 bucket. If not provided, file will have the same name as the source file.')
            ->addArgument('file', InputArgument::REQUIRED, 'File to upload to the S3 bucket');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $bucket = $this->getBucket($input);

            $filepath = $input->getArgument('file');
            @$resource = \fopen($filepath, 'r');
            if (false === $resource) {
                throw new \InvalidArgumentException(sprintf('%s file is not readable or does not exist.', $filepath));
            }

            $client = $this->getS3Client();
            $client->putObject([
                'Bucket' => $bucket,
                'Key' => $input->getOption('s3name') ?? basename($filepath),
                'Body' => $resource,
            ]);

            return 0;
        } finally {
            // Ensure that the file stream is closed regardless.
            if (isset($resource) && is_resource($resource)) {
                \fclose($resource);
            }
        }
    }
}
