<?php

/*
 * This file is part of the Raini AWS package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\AWS\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Command for listing objects in an S3 bucket.
 */
#[AsCommand(
    name: 'aws:s3list',
    description: 'List object in an S3 bucket path.',
    aliases: ['s3ls'],
)]
class S3ListCommand extends AbstractS3Command
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->addOption('max-keys', 'c', InputOption::VALUE_OPTIONAL, 'Maximum number of matching object keys to return', 25)
            ->addArgument('path', InputArgument::OPTIONAL, 'Path to S3 objects for from the selected bucket.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $styler = new SymfonyStyle($input, $output);

        $params = [];
        $params['Bucket'] = $this->getBucket($input);
        $params['Prefix'] = $input->getArgument('path');
        $params['MaxKeys'] = $input->getOption('max-keys');

        $client = $this->getS3Client();
        $result = $client->listObjectsV2(array_filter($params));

        $styler->title(sprintf('Found %d objects:', $result->getKeyCount() ?? 0));

        $rows = [];
        foreach ($result->getContents() as $item) {
            $rows[] = [
                $item->getKey(),
                $item->getSize(),
                $item->getLastModified()->format('Y-m-dTh:i'),
            ];
        }

        // @todo handle prompted pagination.
        $headers = ['Object', 'Filesize', 'Modified'];
        $styler->table($headers, $rows);

        return 0;
    }
}
