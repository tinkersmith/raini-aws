<?php

/*
 * This file is part of the Raini AWS package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\AWS\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command for uploading a file to an S3 bucket.
 */
#[AsCommand(
    name: 'aws:s3download',
    description: 'Downloads a file or resource from a S3 storage bucket.',
    aliases: ['s3dl'],
)]
class S3DownloadCommand extends AbstractS3Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->addArgument('source', InputArgument::REQUIRED, 'File to download from S3 bucket')
            ->addArgument('destination', InputArgument::OPTIONAL, 'Optionally a destination file to upload the file to. If omitted the same name as the S3 filename is used.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $bucket = $this->getBucket($input);
            $src = $input->getArgument('source');
            $dst = $input->getArgument('destination') ?? basename($src);

            $client = $this->getS3Client();
            $result = $client->getObject([
                'Bucket' => $bucket,
                'Key' => $src,
            ]);

            $dstResource = \fopen($dst, 'wb');
            stream_copy_to_stream($result->getBody()->getContentAsResource(), $dstResource);

            return 0;
        } finally {
            // Ensure that the file stream is closed regardless.
            if (isset($dstResource) && is_resource($dstResource)) {
                \fclose($dstResource);
            }
        }
    }
}
