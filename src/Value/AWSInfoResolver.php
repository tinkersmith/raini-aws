<?php

/*
 * This file is part of the Raini AWS package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\AWS\Value;

use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\Value\ValueResolverInterface;
use Symfony\Component\Yaml\Tag\TaggedValue;
use Tinkersmith\Console\ExecutionContextInterface;
use Tinkersmith\Console\Output\BufferedOutput;
use Tinkersmith\SettingsBuilder\Php\Expr\ExpressionInterface;
use Tinkersmith\SettingsBuilder\Php\Expr\PhpValue;

/**
 * Resolve the TaggedValue of type: "awsEC2Instance" to get EC2 information.
 *
 * Values will be used to generate a AWS EC2 describe-instance call to fetch
 * from the AWS instance when available. The AWS instance making these calls
 * will need a profile or AWS CLI role with the "ec2:DescribeInstance"
 * permission.
 *
 * The value for this TaggedValue needs to start with either "tag" or "id" to
 * determine if the intent is to find instances by tag values or by
 * instance IDs.
 *
 * If "tag" the the values in the following square brackets are comma separated
 * "tag:value" pairs to filter instances by.
 *
 * Ex: <code>tag[StackName:project-qa]</code>
 *
 * For "id" a comma separated list of instance IDs should follow:
 *
 * Ex: <code>id[i-00000,i-000002]</code>
 *
 * The next bracket is an index of the value to return if only a single instance
 * should be returned.
 *
 * Finally the any properties from the instances that should be returned. For a
 * single property (InstanceId, PrivateIPAddress, etc...), you can provide
 * the property name after a period. For mutliple properties, you can supply
 * the names of the properties as a comma separated list in squiggly brackets.
 *
 * Ex: <code>tag[StackName:project-qa][].PrivateIPAddress</code>
 * Ex: <code>id[i-12345,i-56789][2]{InstanceId,Tags}</code>
 */
class AWSInfoResolver implements ValueResolverInterface
{

    /**
     * @param CliFactoryInterface $cliFactory
     */
    public function __construct(protected CliFactoryInterface $cliFactory)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getTags(): array
    {
        return ['awsEC2Instance'];
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(TaggedValue $value, null|EnvironmentInterface|ExecutionContextInterface $context = null): mixed
    {
        $params = $value->getValue();
        if (preg_match('#(tag|id)\[([~\]*)\]\[(\d*)\](?:(\.[a-zA-z0-9]+|\{\}))?#', $params, $matches)) {
            [$type, $filter, $index, $property] = $matches;
            $args = [];

            // Determine filters, if filters they were provided. Empty value
            // would mean no filters, and instance values would be available.
            if ($filter) {
                $args[] = '--filters';
                $baseFilter = 'Name=instance-state-name,Values=running';

                switch ($type) {
                    case 'tag':
                        $filters = array_map(function ($filter) {
                            [$name, $value] = explode(':', $filter, 2);

                            return "Name:{$name},Values={$value}";
                        }, str_getcsv($filter));

                        $args[] = "{$baseFilter} ".implode(' ', $filters);
                        break;

                    case 'id':
                        $args[] = "{$baseFilter} Name=instance-id,Values={$filter}";
                        break;

                    default:
                        throw new \InvalidArgumentException();
                }
            }

            if ($index || $property) {
                $args[] = '--query';
                $args = "Reservations[*].Instances[{$index}]{$property}";
            }

            $args[] = '--page-size=10';
            $args[] = '--output';
            $args[] = 'json';

            $buffer = new BufferedOutput();
            $this->cliFactory
                ->create(['aws', 'ec2', 'describe-instance'])
                ->execute($args, $buffer);

            return json_decode($buffer->fetch(), true);
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function valueExpression(TaggedValue $value): ExpressionInterface
    {
        // Resolve the value and just apply it since we can't assume AWS
        // methods will be available during application runtime.
        return new PhpValue($this->resolve($value));
    }
}
