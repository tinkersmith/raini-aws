<?php

/*
 * This file is part of the Raini AWS package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\AWS\Value;

use AsyncAws\SecretsManager\Exception\DecryptionFailureException;
use AsyncAws\SecretsManager\Exception\InternalServiceErrorException;
use AsyncAws\SecretsManager\Exception\ResourceNotFoundException;
use Raini\AWS\SSMParameter;
use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\Extension\ExtensionInterface;
use Raini\Core\Value\ValueResolverInterface;
use Symfony\Component\Yaml\Tag\TaggedValue;
use Tinkersmith\Console\ExecutionContextInterface;
use Tinkersmith\SettingsBuilder\Php\Expr\Expression;
use Tinkersmith\SettingsBuilder\Php\Expr\ExpressionInterface;

/**
 * Resolve the TaggedValue of type: "awsParam" with the AWS SSM Manager.
 */
class SSMParamResolver implements ValueResolverInterface
{

    /**
     * The AWS Extension settings to pass to the value fetchers.
     *
     * @var mixed[]
     */
    protected array $settings;

    /**
     * @param ExtensionManager $extensionManager The extension manager for loading and getting the Raini AWS extension.
     */
    public function __construct(ExtensionInterface $extension)
    {
        $this->settings = $extension->getSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function getTags(): array
    {
        return [
            'awsParam',
            'awsParamGroup',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(TaggedValue $value, null|EnvironmentInterface|ExecutionContextInterface $context = null): mixed
    {
        try {
            switch ($value->getTag()) {
                case 'awsParam':
                    @[$keyname, $property] = explode('/', $value->getValue(), 2);

                    $params = SSMParameter::getParameters($keyname, $this->settings);
                    if ($property && !empty($params[$property])) {
                        return $params[$property]->getValue();
                    }
            }
        } catch (ResourceNotFoundException) {
            throw new \InvalidArgumentException(sprintf('No matching secret is available: %s', $value->getValue()));
        } catch (DecryptionFailureException) {
            throw new \InvalidArgumentException(sprintf('Failed to decrypt secret: %s', $value->getValue()));
        } catch (InternalServiceErrorException $e) {
            throw new \InvalidArgumentException(sprintf('Internal service error: %s', $e->getMessage()));
        }

        throw new \InvalidArgumentException(sprintf('Unable to resolve AWS::SecretsManager for key: %s', $value->getValue()));
    }

    /**
     * {@inheritdoc}
     */
    public function valueExpression(TaggedValue $value): ExpressionInterface
    {
        return new Expression('');
    }
}
